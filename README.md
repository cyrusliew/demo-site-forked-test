# DEMO SITE

This is a bare minimum setup react app for amazing Junior Front End Developer to start showing off their skiils.


### Welcome to Communica's Demo Site test

### Setup
1. Fork this repository to your github account
2. run `yarn install` from project root

### Tasks
Please follow through the given instructions, good luck & happy coding :)

Design and build a single page site which contains the following elements -
1. Navigation
2. Banner slider
3. Feature items
4. Contact form
5. Social links

#### REQUIREMENTS

- Site must be built using Bootstrap framework
- Implement Fontawesome icons
- Stylesheet needs to be written in the format of scss 
